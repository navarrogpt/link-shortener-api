class ShortenedUrlSerializer < BaseSerializer
  attributes  :id,
              :url,
              :unique_key,
              :display,
              :visits,
              :total_visits

  def display
    ENV['BASE_URL'] + object.unique_key
  end

  def visits
    Impression.where(impressionable_id: object.id)
  end

  def total_visits
    Impression.where(impressionable_id: object.id).count
  end

end
