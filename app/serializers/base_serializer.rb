class BaseSerializer < ActiveModel::Serializer
  cache
  delegate :key, to: :object
end
