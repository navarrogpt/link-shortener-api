require 'uuidtools'
require 'base64'

class ShortenedUrl < ActiveRecord::Base
  is_impressionable
  after_create :generate_slug

  def generate_slug
    #self.unique_key = self.id.to_s(36)
    self.unique_key = Base64.encode64(UUIDTools::UUID.random_create)[0..5]
    self.save
  end

  def display_slug
    ENV['BASE_URL'] + self.unique_key
  end
end
