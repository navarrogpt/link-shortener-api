module Api
  module V1
    class ApiController < ApplicationController
      include LinkShortenerApi::StrongParameters
    end
  end
end