module Api
  module V1
    class ShortenedUrlsController < ApiController
      impressionist :actions => [:open_link]
      
      def index
        @shortened_urls = ShortenedUrl.all
        render json: @shortened_urls
      end

      def show
        @shortened_url = ShortenedUrl.find(params[:id])
        render json: @shortened_url
      end

      def open_link
        @shortened_url = ShortenedUrl.find(params[:id])
        render json: @shortened_url
      end

      def create
        # @shortened_url = ShortenedUrl.new(shortened_url_params)
        # puts shortened_url_params[:url]
        # if shortened_url_params[:url]
        #   @shortened_url = Shortener::ShortenedUrl.generate(shortened_url_params[:url])
        #   render json: @shortened_url, status: :created
        # else
        #   render json: @shortened_url.errors, status: unprocessable_entity
        # end

        @shortened_url = ShortenedUrl.new(shortened_url_params)
        if @shortened_url.save
          render json: @shortened_url, status: :created
        else
          render json: @shortened_url.errors, status: unprocessable_entity
        end
      end

      private

        def shortened_url_params
          params.require(:shortened_url)
          params.require(:shortened_url).permit(permitted_shortened_url_attributes)
        end
    end
  end
end