class ChangeShortenedUrlUniqueKeyColumnToNull < ActiveRecord::Migration
	def change
	  change_column_null(:shortened_urls, :unique_key, true)
	end
end
