module LinkShortenerApi
  module PermittedAttributes
    ATTRIBUTES = [
      :shortened_url_attributes
    ]

    mattr_reader *ATTRIBUTES

    @@shortened_url_attributes = [:url]
  end
end