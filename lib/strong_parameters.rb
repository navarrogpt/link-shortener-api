require 'permitted_attributes'

module LinkShortenerApi
  module StrongParameters
    def permitted_attributes
      LinkShortenerApi::PermittedAttributes
    end

    delegate *LinkShortenerApi::PermittedAttributes::ATTRIBUTES,
             to: :permitted_attributes,
             prefix: :permitted

    def permitted_shortened_url_attributes
      permitted_attributes.shortened_url_attributes
    end

  end
end
